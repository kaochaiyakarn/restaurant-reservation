const router = require("express").Router();
const config = require("../configs/app");
console.log(config.apiVersion)
router.use(`/v${config.apiVersion}/siampiwat/challenge`,
    require("./apis/HealthCheck"),
    require("./apis/InitialTable"),
    require("./apis/TableReservation")
);

module.exports = router;