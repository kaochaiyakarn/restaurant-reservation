const router = require('express').Router();

router.use("/tableReservation", require("./tableReservation.routes"));
module.exports = router;