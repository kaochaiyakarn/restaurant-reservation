const router = require("express").Router();
const tableReservation = require("../../../controllers/TableReservation/tableReservation.controllers")
const initialTableValidator = require("../../../validations/TableReservation/tableReservation.validation")
const initialTableMiddleWare = require("../../../validations/TableReservation/tableReservation.middleware")
const { checkSchema } = require("express-validator");

// reserve table
router.post(
    '/reserveTable',
    checkSchema(initialTableValidator.ReserveTableBodyValidator),
    initialTableMiddleWare.validateInitialTable,
    tableReservation.reserveTable
)

// cancel table
router.post(
    '/cancelTable',
    checkSchema(initialTableValidator.CancelTableBodyValidator),
    initialTableMiddleWare.validateInitialTable,
    tableReservation.cancelTable
)

// get table
router.get(
    '/getTableInformation',
    initialTableMiddleWare.validateInitialTable,
    tableReservation.getAvaliableTable
)

module.exports = router;