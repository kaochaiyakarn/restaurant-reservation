const router = require("express").Router();
const initialTable = require("../../../controllers/InitialTable/initialTable.controllers")
const initialTableValidator = require("../../../validations/InitialTable/initialTable.validation")
const { checkSchema } = require("express-validator");

router.post(
    '/',
    checkSchema(initialTableValidator.initialTableBodyValidator),
    initialTable.postInitialTable,
)

module.exports = router;