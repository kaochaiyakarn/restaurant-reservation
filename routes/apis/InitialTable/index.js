const router = require('express').Router();

router.use("/initialTable", require("./initialTable.routes"));
module.exports = router;