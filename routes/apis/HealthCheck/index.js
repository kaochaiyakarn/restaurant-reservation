const router = require('express').Router();

router.use("/healthcheck", require("./healthCheck.routes"));
module.exports = router;