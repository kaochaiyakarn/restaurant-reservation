const router = require("express").Router();
const healthCheckController = require("../../../controllers/HealthCheck/healthCheck.controllers")
router.get(
    '/',
    healthCheckController.getHealthCheck,
)

module.exports = router;