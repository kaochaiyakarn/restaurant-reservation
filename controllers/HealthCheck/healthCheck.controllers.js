const serviceName = "HEALTHCHECK";
const service = require("../../services/HealthCheck/healthCheck.service")
const methods = {
  async getHealthCheck(req, res, next) {
    let healthStatus = await service.getHealthCheck().catch(err => {
      return res.status(503).json({status: "Service Temporary down"})
    })
    return res.status(200).json(healthStatus);
  },
};

module.exports = { ...methods };
