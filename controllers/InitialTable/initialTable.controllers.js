const serviceName = "INITIALTABLE";
const service = require("../../services/InitialTable/initialTable.service")
const { validationResult } = require('express-validator');

const methods = {
    async postInitialTable(req, res, next) {

        // valdiate Request
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
            });
        }

        // call service
        let error = undefined;
        let serviceResponse = await service.initialTable(req.body.tableSize).catch(async (err) => {
            error = err
        });
        return error === undefined ? res.status(201).json(serviceResponse): res.status(error.status).json({msg: error.message});
    },
};

module.exports = { ...methods };
