const serviceName = "RESERVATIONTABLE";
const service = require("../../services/TableReservation/tableReservation.service")
const { validationResult } = require('express-validator');



const methods = {
    async reserveTable(req, res, next) {

        // valdiate Request
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
            });
        }
        // call service
        let error = undefined;
        let serviceResponse = await service.reserveTable(req.body.custormerSize).catch(async (err) => {
            error = err
        });
        return error === undefined ? res.status(200).json(serviceResponse): res.status(error.status).json({msg: error.message});
    },

    async cancelTable(req, res, next){
        // valdiate Request
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
            });
        }
        // call service
        let error = undefined;
        let serviceResponse = await service.cancelTable(req.body.bookingId).catch(async (err) => {
            error = err
        })
        return error === undefined ? res.status(200).json(serviceResponse): res.status(error.status).json({msg: error.message});
    },

    async getAvaliableTable(req, res, next){
        // call service
        let error = undefined;
        let serviceResponse = await service.getAvaliableTable()
        return res.status(200).json(serviceResponse)
    }
};

module.exports = { ...methods };
