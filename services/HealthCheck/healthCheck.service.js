const methods = {
    async getHealthCheck(){
        const healthStatus = {
            uptime: process.uptime(),
            status: "OK",
            timestamp: Date.now()
        }

        return healthStatus
    }
}

module.exports = { ...methods}