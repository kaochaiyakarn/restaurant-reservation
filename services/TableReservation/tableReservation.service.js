const { ErrorForbidden, ErrorBadRequest } = require("../../helpers/ErrorHandlers/errorMethods")
const { v4 : uuidv4 } = require('uuid');

const methods = {
    async reserveTable(custormerSize){ 
        let requireTable = Math.ceil(custormerSize/4)

        if (global.tableInformation.get('avaliable').length < requireTable){
            throw ErrorForbidden("No Avaliable Table at a moment")
        }

        let tempAvaliableTableId = global.tableInformation.get('avaliable')
        let reservedTableId= tempAvaliableTableId.splice(0, requireTable)

        global.tableInformation.set('avaliable', tempAvaliableTableId)
        global.tableInformation.set('reserved', [...global.tableInformation.get('reserved'), ...reservedTableId])

        let bookingId = uuidv4()
        global.tableInformation.set(bookingId, reservedTableId)

        return {
            bookingId: bookingId,
            bookedTableSize: requireTable,
            listOfBookedTableId: reservedTableId,
            remainingTable: global.tableInformation.get('avaliable').length
        }
    },

    async cancelTable(bookId){
        // This API must be called after initialization, otherwise, return error.
        // b. Booking ID has to be sent when calling this API.
        // c. All tables belonging to the given Booking ID must be released and available for
        // the other reservations.
        // d. Return success with a number of freed tables for this reservation, and a number
        // of remaining tables.
        // e. Return error if Booking ID is not found.
        let reservedTable = global.tableInformation.get(bookId);
        if (reservedTable === undefined){
            throw ErrorBadRequest("Booking ID not found")
        }

        global.tableInformation.set('avaliable', [...global.tableInformation.get('avaliable'), ...reservedTable])

        let uniqueTable = global.tableInformation.get('reserved').filter((val) => reservedTable.indexOf(val) === -1)
        console.log("uniqueTable: ", uniqueTable)
        global.tableInformation.set('reserved', uniqueTable)

        global.tableInformation.delete(bookId)
        
        return {
            bookingId: bookId,
            freedTableSize: reservedTable.length,
            remainingTable: global.tableInformation.get('avaliable').length
        }
    },

    async getAvaliableTable(){
        return {
            remainingTable: global.tableInformation.get('avaliable').length,
            avaliableTableList: global.tableInformation.get('avaliable'),
            reservedTable: global.tableInformation.get('reserved').length,
            reservedTableList: global.tableInformation.get('reserved')

        }
    }
}

module.exports = { ...methods}