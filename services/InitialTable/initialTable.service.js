const { ErrorBadRequest } = require("../../helpers/ErrorHandlers/errorMethods")
const methods = {
    async initialTable(tableSize){ 

        // REQ:
        // Create an API to initialize all tables in the restaurant.
        // a. This API must be always called first and only once at the beginning.
        // b. A number of tables in the restaurant has to be sent when calling this API for
        // initialization.
        // c. Return success if tables are initialized successfully.
        // d. Return error if this API is called again after initialization.
        
        if (global.tableInformation.get('avaliable').length > 0 || global.tableInformation.get('reserved').length > 0){
            // throw error
            throw ErrorBadRequest("Table is already initialized")
        }

        for (var i = 0; i<tableSize; i++){
            global.tableInformation.set('avaliable', [...Array(tableSize).keys()])
        }

        return {status: "Table initialized successfully", tableSize: global.tableInformation.get('avaliable').length}
    }
}

module.exports = { ...methods}