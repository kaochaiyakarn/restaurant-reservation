const express = require('express'),
    app = express(),
    config = require('./configs/app');

// Export app Configs
require("./configs/express/express.config")(app);

// Export Routes
app.use(require("./routes"));
// require("./helpers/ErrorHandlers/errorHandler")(config.isProduction, app);

global.tableInformation = new Map();
global.tableInformation.set("avaliable", [])
global.tableInformation.set("reserved", [])

// Start Server
const server = app.listen(config.port, () => {
    let host = server.address().address
    let port = server.address().port
    console.log(`Server is starting at http://${host}:${port}`)
})

module.exports = server