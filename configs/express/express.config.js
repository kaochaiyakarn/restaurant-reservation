const express = require('express');
const cors = require('cors');

module.exports = async (app) => {
    app.use(cors());
    app.use(express.json({limt: "50mb"}))
    app.use(
        express.urlencoded({
          limit: "50mb",
          extended: true,
        })
    ); 
}