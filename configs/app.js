require('dotenv').config();

module.exports = {
    port: process.env.PORT || 3000,
    apiVersion: process.env.APIVERSION || "1",
    serviceVersion: process.env.SERVICEVERSION || "1"
}