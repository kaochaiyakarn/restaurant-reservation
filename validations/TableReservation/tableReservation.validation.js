const ReserveTableBodyValidator = {
    custormerSize: {
        exists: {
            errorMessage: "custormerSize is required"
        },
        isInt: { errorMessage: "custormerSize should be integer" }
    }
};

const CancelTableBodyValidator = {
    bookingId: {
        exists: {
            errorMessage: "BookingId is required"
        },
        isString: {errorMessage: "bookingId should be string"}
    }
}



module.exports = {
    ReserveTableBodyValidator,
    CancelTableBodyValidator
};