const methods = {
    async validateInitialTable(req, res, next){
        if (global.tableInformation.get('avaliable').length + global.tableInformation.get("reserved").length < 1){
            return res.status(400).json({status:400, msg: "Table was not initialize"})
        }
        next()
    }
}


module.exports = { ...methods };