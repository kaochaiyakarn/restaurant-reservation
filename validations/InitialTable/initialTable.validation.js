const initialTableBodyValidator = {
    tableSize: {
        exists: {
            errorMessage: "tableSize is required"
        },
        isInt: { errorMessage: "tableSize should be integer"}
    },
  };


module.exports = {
    initialTableBodyValidator
  };