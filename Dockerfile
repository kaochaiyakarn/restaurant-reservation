FROM node:18-alpine3.14
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# COPY ./package.json .
COPY package*.json yarn.lock ./
RUN yarn --pure-lockfile

# RUN yarn global add nodemon
# RUN yarn add --quiet
# RUN npm install express --save
# RUN yarn add express --save
RUN yarn install
COPY . /usr/src/app

CMD [ "yarn", "start:prod" ]

