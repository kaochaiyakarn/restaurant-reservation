const service = require("../../services/InitialTable/initialTable.service")
const chai = require("chai")
const expect = chai.expect;

it("[SUCCESS-CASE01]: First time call with tableSize = 2", async function() {
    const tableSize = 2
    let returnJson =  await service.initialTable(tableSize)
    expect(returnJson.status).to.equal("Table initialized successfully")
    expect(returnJson.tableSize).to.equal(tableSize)
})

it("[SUCCESS-CASE02]: First time call with tableSize = 10", async function() {
    const tableSize = 10
    let returnJson =  await service.initialTable(tableSize)
    expect(returnJson.status).to.equal("Table initialized successfully")
    expect(returnJson.tableSize).to.equal(tableSize)
})

it("[Fail-CASE01]: Second time call For fail", async function() {
    const tableSize = 2
    let returnJson =  await service.initialTable(tableSize)
    
    let error = undefined
    returnJson =  await service.initialTable(tableSize).catch(async (err) =>{
        error = err
    })
    expect(error.message).to.equal("Table is already initialized")
    expect(error.status).to.equal(400)
})