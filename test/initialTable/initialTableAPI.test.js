const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../index');
const expect = chai.expect;
chai.use(chaiHttp);

it("[SUCCESS-CASE01]: First time call with tableSize = 2", async function () {
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: 2
        })
    expect(response.status).to.equal(201)
    expect(response.body.tableSize).to.equal(2)
})

it("[FAILED-CASE01]: First time call with tableSize = 2", async function () {
    let firstResponse = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: 2
        })
    let secondResponse = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: 2
        })
    expect(secondResponse.status).to.equal(400)
})

it("[FAILED-CASE02]: tableSize invalid type", async function () {
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: true
        })
    expect(response.status).to.equal(400)
})
