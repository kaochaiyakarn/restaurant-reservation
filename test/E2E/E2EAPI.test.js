const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../index');
const expect = chai.expect;
chai.use(chaiHttp);


it("[SUCCESS-CASE01]: Initial 2 table book 2 time then cancel 2 time", async function () {
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: 2
        })
    expect(response.status).to.equal(201)
    expect(response.body.tableSize).to.equal(2)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 2
        })
    
    let bookingId1 = response.body.bookingId
    expect(response.status).to.equal(200)
    expect(response.body.bookedTableSize).to.equal(1)
    expect(response.body.remainingTable).to.equal(1)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 4
        })
    let bookingId2 = response.body.bookingId
    expect(response.status).to.equal(200)
    expect(response.body.bookedTableSize).to.equal(1)
    expect(response.body.remainingTable).to.equal(0)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: bookingId1
        })
    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.equal(bookingId1)
    expect(response.body.freedTableSize).to.equal(1)
    expect(response.body.remainingTable).to.equal(1)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: bookingId2
        })
    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.equal(bookingId2)
    expect(response.body.freedTableSize).to.equal(1)
    expect(response.body.remainingTable).to.equal(2)
})

it("[SUCCESS-CASE01]: Complex Case: Screnario provide below", async function () {
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/initialTable')
        .send({
            tableSize: 3
        })
    expect(response.status).to.equal(201)
    expect(response.body.tableSize).to.equal(3)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 6
        })
    
    let bookingId1 = response.body.bookingId
    expect(response.status).to.equal(200)
    expect(response.body.bookedTableSize).to.equal(2)
    expect(response.body.remainingTable).to.equal(1)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 8
        })
    expect(response.status).to.equal(403)

    response = await chai.request(server)
    .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
    .send({
        bookingId: bookingId1
    })
    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.equal(bookingId1)
    expect(response.body.freedTableSize).to.equal(2)
    expect(response.body.remainingTable).to.equal(3)

    response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 9
        })
    
    let bookingId2 = response.body.bookingId
    expect(response.status).to.equal(200)
    expect(response.body.bookedTableSize).to.equal(3)
    expect(response.body.remainingTable).to.equal(0)
})

// COMPLEX CASE:
// Initial 3 table
// book 2 table
// book againg with 2 table = fail
// cancel 2 table
// book 3 table
// cancel 3 table