function importTest(testName, testPath) {
    describe(testName, function () {
        require(testPath)
    })
}

// HealthCheck Service
describe("HealthCheck Service", function () {
    // Test service
    importTest("HealthCheckService resposne", "./healthCheck/healthCheckService.test")
    // Test http call
    importTest("HealthCheckService resposne", "./healthCheck/healthCheckAPI.test")
})

// InitialTable Service
describe("Initial Table Service", function () {

    beforeEach(function () {
        global.tableInformation = new Map();
        global.tableInformation.set("avaliable", [])
        global.tableInformation.set("reserved", [])
    });
    // Test service
    importTest("Table Service resposne", "./initialTable/initialTableService.test")
    // Test http call
    importTest("Table Service resposne", "./initialTable/initialTableAPI.test")
})

// reservationtable Service
describe("Reservation Table Service", function () {

    beforeEach(function () {
        global.tableInformation = new Map();
        global.tableInformation.set("avaliable", [])
        global.tableInformation.set("reserved", [])
    });

    // Test service
    importTest("Table Service resposne", "./TableReservation/reserveTable/reserveTableService.test")
    // Test http call
    importTest("Table Service resposne", "./TableReservation/reserveTable/reserveTableAPI.test")
})

// Cancel Reservation Service
describe("Cancellation Table Service", function () {

    beforeEach(function () {
        global.tableInformation = new Map();
        global.tableInformation.set("avaliable", [])
        global.tableInformation.set("reserved", [])
    });

    // Test service
    importTest("Table Service resposne", "./TableReservation/cancelTable/cancelTableService.test")
    // Test http call
    importTest("Table Service resposne", "./TableReservation/cancelTable/cancelTableAPI.test")
})

// Get Reservation Service
describe("Get Table Service", function () {

    beforeEach(function () {
        global.tableInformation = new Map();
        global.tableInformation.set("avaliable", [])
        global.tableInformation.set("reserved", [])
    });

    // Test service
    importTest("Table Service resposne", "./TableReservation/getTable/getTableInformationService.test")
    // Test http call
    importTest("Table Service resposne", "./TableReservation/getTable/getTableInformationAPI.test")
})

describe("E@E", function () {
    // Test http call
    beforeEach(function () {
        global.tableInformation = new Map();
        global.tableInformation.set("avaliable", [])
        global.tableInformation.set("reserved", [])
    });
    importTest("Table Service resposne", "./E2E/E2EAPI.test")
})