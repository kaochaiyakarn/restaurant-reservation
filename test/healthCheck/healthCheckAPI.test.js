const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../index');
const expect = chai.expect;
chai.use(chaiHttp);

it("[SUCCESS-CASE01]: Health chek", async function () {
    let response = await chai.request(server)
        .get('/v1/siampiwat/challenge/healthcheck')

    expect(response.status).to.equal(200)
    expect(response.body.status).to.equal("OK")
    expect(response.body.uptime).greaterThan(0)
})



