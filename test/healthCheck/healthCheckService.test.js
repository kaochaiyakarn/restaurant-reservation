const service = require("../../services/HealthCheck/healthCheck.service")
const chai = require("chai")
const expect = chai.expect;

it("[SUCCESS-CASE]: getHealthCheckService have to return uptime>0, status ok", async function() {
    let healthCheckStatus =  await service.getHealthCheck()
    expect(healthCheckStatus.status).to.equal("OK")
    expect(healthCheckStatus.uptime).greaterThan(0)
})