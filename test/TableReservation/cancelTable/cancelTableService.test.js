const service = require("../../../services/TableReservation/tableReservation.service")
const { v4 : uuidv4 } = require('uuid');
const chai = require("chai")
const expect = chai.expect;

it("[SUCCESS-CASE01]: Book 1 table from 2 table then cancel 1 table", async function() {
    global.tableInformation.set("avaliable", [2])
    global.tableInformation.set("reserved", [1])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1])
    
    let returnJson = await service.cancelTable(bookingId)
    expect(returnJson.bookingId).to.equal(bookingId)
    expect(returnJson.freedTableSize).to.equal(1)
    expect(returnJson.remainingTable).to.equal(2)
    expect(global.tableInformation.get('avaliable').length).to.equal(2)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[SUCCESS-CASE02]: Book 2 table from 3 table then cancel 2 table", async function() {
    global.tableInformation.set("avaliable", [3])
    global.tableInformation.set("reserved", [1,2])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1,2])
    
    let returnJson = await service.cancelTable(bookingId)
    expect(returnJson.bookingId).to.equal(bookingId)
    expect(returnJson.freedTableSize).to.equal(2)
    expect(returnJson.remainingTable).to.equal(3)
    expect(global.tableInformation.get('avaliable').length).to.equal(3)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[FAILED-CASE01]: Cancel BookId undefinded", async function() {
    global.tableInformation.set("avaliable", [2])
    global.tableInformation.set("reserved", [1])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1])
    
    let error = undefined
    let returnJson = await service.cancelTable("undefinedBookId").catch(async (err) =>{
        error = err
    })

    expect(error.message).to.equal("Booking ID not found")
    expect(error.status).to.equal(400)

    expect(global.tableInformation.get('avaliable').length).to.equal(1)
    expect(global.tableInformation.get('reserved').length).to.equal(1)
})