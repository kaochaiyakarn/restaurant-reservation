const { v4 : uuidv4 } = require('uuid');
const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../../index');
const expect = chai.expect;
chai.use(chaiHttp);

it("[SUCCESS-CASE01]: Book 1 table for 2 custormer then cancel", async function () {

    global.tableInformation.set("avaliable", [2])
    global.tableInformation.set("reserved", [1])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: bookingId
        })

    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.equal(bookingId)
    expect(response.body.freedTableSize).to.equal(1)
    expect(response.body.remainingTable).to.equal(2)

    expect(global.tableInformation.get('avaliable').length).to.equal(2)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[SUCCESS-CASE01]: Book 2 table for 7 custormer then cancel", async function () {

    global.tableInformation.set("avaliable", [3])
    global.tableInformation.set("reserved", [1,2])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1,2])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: bookingId
        })

    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.equal(bookingId)
    expect(response.body.freedTableSize).to.equal(2)
    expect(response.body.remainingTable).to.equal(3)

    expect(global.tableInformation.get('avaliable').length).to.equal(3)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[FAILED-CASE01]: Cancel undefine bookid", async function () {

    global.tableInformation.set("avaliable", [3])
    global.tableInformation.set("reserved", [1,2])

    const bookingId = uuidv4()
    global.tableInformation.set(bookingId, [1,2])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: "undefinedBookingId"
        })

    expect(response.status).to.equal(400)
    expect(response.body.msg).to.equal("Booking ID not found")

    expect(global.tableInformation.get('avaliable').length).to.equal(1)
    expect(global.tableInformation.get('reserved').length).to.equal(2)

})

it("[FAILED-CASE02]: Book table while wasn't initial table", async function () {

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: "id"
        })
    expect(response.status).to.equal(400)
    expect(response.body.msg).to.equal("Table was not initialize")

    expect(global.tableInformation.get('avaliable').length).to.equal(0)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[FAILED-CASE03]: bookingId invalid type", async function () {
    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/cancelTable')
        .send({
            bookingId: true
        })
    expect(response.status).to.equal(400)
})