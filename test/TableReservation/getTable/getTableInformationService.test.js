const service = require("../../../services/TableReservation/tableReservation.service")
const chai = require("chai")
const expect = chai.expect;

it("[SUCCESS-CASE01]: Get Table information", async function() {
    global.tableInformation.set("avaliable", [2,3])
    global.tableInformation.set("reserved", [1])
    
    let returnJson = await service.getAvaliableTable()
    expect(returnJson.remainingTable).to.equal(2)
    expect(returnJson.avaliableTableList).to.equal(global.tableInformation.get("avaliable"))
    expect(returnJson.reservedTable).to.equal(1)
    expect(returnJson.reservedTableList).to.equal(global.tableInformation.get("reserved"))
})