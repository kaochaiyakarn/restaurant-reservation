const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../../index');
const expect = chai.expect;
chai.use(chaiHttp);

it("[SUCCESS-CASE01]: Get Tableinformation", async function () {

    global.tableInformation.set("avaliable", [2,3])
    global.tableInformation.set("reserved", [1])

    let response = await chai.request(server)
        .get('/v1/siampiwat/challenge/tableReservation/getTableInformation')
    expect(response.status).to.equal(200)
    expect(response.body.remainingTable).to.equal(2)
    expect(response.body.avaliableTableList.toString()).to.equal(global.tableInformation.get("avaliable").toString())
    expect(response.body.reservedTable).to.equal(1)
    expect(response.body.reservedTableList.toString()).to.equal(global.tableInformation.get("reserved").toString())
})

it("[FAILED-CASE01]: Book table while wasn't initial table", async function () {

    let response = await chai.request(server)
        .get('/v1/siampiwat/challenge/tableReservation/getTableInformation')


    expect(response.status).to.equal(400)
    expect(response.body.msg).to.equal("Table was not initialize")

    expect(global.tableInformation.get('avaliable').length).to.equal(0)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})