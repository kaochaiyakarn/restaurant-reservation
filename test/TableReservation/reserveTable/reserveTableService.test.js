const service = require("../../../services/TableReservation/tableReservation.service")
const chai = require("chai")
const expect = chai.expect;

it("[SUCCESS-CASE01]: Book 1 table from 2 table", async function() {
    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    const custormerSize = 2
    let returnJson =  await service.reserveTable(custormerSize)
    expect(returnJson.bookingId).to.not.equal(undefined)
    expect(returnJson.bookedTableSize).to.equal(1)
    expect(returnJson.listOfBookedTableId).to.be.an('array').that.includes(1);

    expect(global.tableInformation.get('avaliable').length).to.equal(1)
    expect(global.tableInformation.get('reserved').length).to.equal(1)
})

it("[SUCCESS-CASE02]: Book 2 table from 2 table", async function() {
    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    const custormerSize = 6
    let returnJson =  await service.reserveTable(custormerSize)
    expect(returnJson.bookingId).to.not.equal(undefined)
    expect(returnJson.bookedTableSize).to.equal(2)
    expect(returnJson.listOfBookedTableId).to.be.an('array').that.includes(1);
    expect(returnJson.listOfBookedTableId).to.be.an('array').that.includes(2);

    expect(global.tableInformation.get('avaliable').length).to.equal(0)
    expect(global.tableInformation.get('reserved').length).to.equal(2)
})

it("[Failed-CASE01]: Book 3 table for 9 person from 2 table", async function() {
    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    const custormerSize = 9
    let error = undefined

    let returnJson =  await service.reserveTable(custormerSize).catch(async (err) =>{
        error = err
    })

    expect(error.message).to.equal("No Avaliable Table at a moment")
    expect(error.status).to.equal(403)

    expect(global.tableInformation.get('avaliable').length).to.equal(2)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

