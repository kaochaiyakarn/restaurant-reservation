const chai = require("chai")
let chaiHttp = require('chai-http');
let server = require('../../../index');
const expect = chai.expect;
chai.use(chaiHttp);

it("[SUCCESS-CASE01]: Book 1 table for 2 custormer", async function () {

    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 2
        })
    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.not.equal(undefined)
    expect(response.body.bookedTableSize).to.equal(1)
    expect(response.body.listOfBookedTableId).to.be.an('array').that.includes(1);
    expect(response.body.remainingTable).to.equal(1)

    expect(global.tableInformation.get('avaliable').length).to.equal(1)
    expect(global.tableInformation.get('reserved').length).to.equal(1)
})

it("[SUCCESS-CASE02]: Book 2 tables for 6 custormer", async function () {

    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 6
        })
    expect(response.status).to.equal(200)
    expect(response.body.bookingId).to.not.equal(undefined)
    expect(response.body.bookedTableSize).to.equal(2)
    expect(response.body.listOfBookedTableId).to.be.an('array').that.includes(1);
    expect(response.body.listOfBookedTableId).to.be.an('array').that.includes(2);

    expect(global.tableInformation.get('avaliable').length).to.equal(0)
    expect(global.tableInformation.get('reserved').length).to.equal(2)
})

it("[FAILED-CASE01]: Having 2 table for 8 person while calling with 10 person which we have to preapre 3 table", async function () {

    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 10
        })
    expect(response.status).to.equal(403)

    expect(global.tableInformation.get('avaliable').length).to.equal(2)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[FAILED-CASE02]: Book table while wasn't initial table", async function () {

    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: 10
        })
    expect(response.status).to.equal(400)
    expect(response.body.msg).to.equal("Table was not initialize")

    expect(global.tableInformation.get('avaliable').length).to.equal(0)
    expect(global.tableInformation.get('reserved').length).to.equal(0)
})

it("[FAILED-CASE03]: CustormerSize invalid type", async function () {
    global.tableInformation.set("avaliable", [1,2])
    global.tableInformation.set("reserved", [])
    let response = await chai.request(server)
        .post('/v1/siampiwat/challenge/tableReservation/reserveTable')
        .send({
            custormerSize: "string"
        })
    expect(response.status).to.equal(400)
})



