# restaurant-reservation



## Getting started

### option 1
```
yarn install
yarn start
```

### option 2
```
docker-compose up -d (If you prefer to allow docker running in backgroud)
docker-compose up (It will show realtime thing in the terminal)
```


## PostMan API Specification
[PostmanLink](https://documenter.getpostman.com/view/10286442/Uz5GpGRW)

## Folder Structure

. <br>
├── Dockerfile <br>
├── README.md <br>
├── configs: Config of BE service express <br>
│   ├── app.js <br>
│   └── express <br>
│       └── express.config.js <br>
├── controllers Controller of each endpoint <br>
│   ├── HealthCheck <br>
│   │   └── healthCheck.controllers.js <br>
│   ├── InitialTable <br>
│   │   └── initialTable.controllers.js <br>
│   └── TableReservation <br>
│       └── tableReservation.controllers.js <br>
├── docker-compose.yml <br>
├── helpers Helper for manage Error and Response (Response isn't complete) <br>
│   ├── ErrorHandlers <br>
│   │   └── errorMethods.js <br>
│   └── responseHandlers <br>
│       └── responseHandlers.js <br>
├── index.js <br>
├── package.json <br>
├── routes Router for navigate to middleware, validator and controller for each endpoint <br>
│   ├── apis <br>
│   │   ├── HealthCheck <br>
│   │   │   ├── healthCheck.routes.js <br>
│   │   │   └── index.js <br>
│   │   ├── InitialTable <br>
│   │   │   ├── index.js <br>
│   │   │   └── initialTable.routes.js <br>
│   │   └── TableReservation <br>
│   │       ├── index.js <br>
│   │       └── tableReservation.routes.js <br>
│   └── index.js <br>
├── services <br>
│   ├── HealthCheck <br>
│   │   └── healthCheck.service.js <br>
│   ├── InitialTable <br>
│   │   └── initialTable.service.js <br>
│   └── TableReservation <br>
│       └── tableReservation.service.js <br>
├── test Testing part, I separate into two thing. First, I test the service file. Second, I test with mockup service <br>
│   ├── E2E <br>
│   │   └── E2EAPI.test.js <br>
│   ├── TableReservation <br>
│   │   ├── cancelTable <br>
│   │   │   ├── cancelTableAPI.test.js <br>
│   │   │   └── cancelTableService.test.js <br>
│   │   ├── getTable <br>
│   │   │   ├── getTableInformationAPI.test.js <br>
│   │   │   └── getTableInformationService.test.js <br>
│   │   └── reserveTable <br>
│   │       ├── reserveTableAPI.test.js <br>
│   │       └── reserveTableService.test.js <br>
│   ├── healthCheck <br>
│   │   ├── healthCheckAPI.test.js <br>
│   │   └── healthCheckService.test.js <br>
│   ├── initialTable <br>
│   │   ├── initialTableAPI.test.js <br>
│   │   └── initialTableService.test.js <br>
│   └── unittest.main.js <br>
├── test-run.xml <br>
├── validations <br>
│   ├── InitialTable <br>
│   │   └── initialTable.validation.js <br>
│   └── TableReservation <br>
│       ├── tableReservation.middleware.js <br>
│       └── tableReservation.validation.js <br>
└── yarn.lock <br>


