module.exports = {
    ErrorBadRequest(msg) {
      let error = new Error(msg)
      error.message = msg
      error.status = 400
      return error
    },
    ErrorForbidden(msg) {
      let error = new Error(msg)
      error.message = msg
      error.status = 403
      return error
    },
    ErrorInternalServerError(msg) {
      let error = new Error(msg)
      error.message = msg
      error.status = 500
      return error
    },
  }
  